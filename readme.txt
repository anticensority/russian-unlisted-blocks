Внереестровые блокировки в России.

В этом документе перечислены интернет-ресурсы, заблокированные в России на оборудовании ТСПУ, но не внесённые в Реестр запрещенных сайтов официальным образом.

Множество IP-адресов Tor-нод (Tor Relay)
https://tor.eff.org (блокировка по HTTPS SNI, только порт 443)
~~theins.ru~~ (Уже в Реестре. HTTP и HTTPS. СМИ сообщали о блокировке по ссылкой на Роскомнадзор, но адрес так и не был внесён в реестр.)
https://news.google.com (только HTTPS SNI. СМИ сообщали о блокировке по ссылкой на Роскомнадзор, но адрес так и не был внесён в реестр.)
https://play.google.com (только HTTPS SNI)
*.twimg.com (HTTP и HTTPS, например pbs.twimg.com)
IP-адреса сервиса Cloudflare: 188.114.96.2, 188.114.97.2, 188.114.96.7, 188.114.97.7 (см. https://ntc.party/t/cloudflare-cdn/2245/)
~~IP-адрес 216.239.36.54 сервиса Google Cloud Functions и адреса 199.36.158.100 сервиса Google Firebase Hosting (см. https://ntc.party/t/google-cloud-functions/2247/)~~
Блокировка протокола QUIC для зарубежных направлений (см. https://ntc.party/t/http-3-quic/1823/)
Некоторые популярные VPN-сервисы
~~acf.international~~ (уже в Реестре)
bbc.co.uk / bbci.co.uk
*.radiojar.com (только поддомены, например, stream.radiojar.com или n0f.radiojar.com. На основном radiojar.com сам сайт разрывает соединение. С этого домена вещает Радио Свобода.)
www.xvideos.com (блокировка порта 443 на IP-адресах домена, хотя в Реестре только http-ссылки, без записей о блокировке IP. Пример: 185.88.181.5, 185.88.181.6, 185.88.181.10).
doubleclick.net (реклама Google)
https://*.windscribe.com (на любой IP-адрес)
vpngate.net
rebrand.ly
adguard.com
antizapret.prostovpn.org
avira.com
mullvad.net
invent.kde.org (блокировка порта 443 IPv4-адреса 188.40.133.145, IPv6 доступен)
s-trade.com
*.ua
is.gd
1plus1tv.ru
linktr.ee
is.gd
anicult.org
12putinu.net
padlet.com
*.tlsext.com